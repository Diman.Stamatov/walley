﻿namespace Walley.Models
{
    public class cryptoCoin
    {
        private double amount;

        private string name;

        private decimal originalPrice; 

        public double Amount { 
            
            get 
            { 
                return this.amount; 
            }  
            private set
            {
                this.amount = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }

            set
            { 
                this.name = value; 
            }
        }

        public decimal OriginalPrice
        {
            get
            {
                return this.originalPrice;
            }

            set
            { 
                this.originalPrice = value; 
            }

        }


    }
}
